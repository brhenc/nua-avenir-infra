# Bootstrapping a system

Booting and installing the initial system goes here.

As a rule of thumb, anything needed to get a working SSH connection goes
here as long as it is software related. Hardware setup goes to hardware,
and networking setup (from cabling to switching to PXE boot) goes to
networking).

Terraform scripts belong here as well, as do APIs doing the heavy lifting
to automatically provision VMs.
