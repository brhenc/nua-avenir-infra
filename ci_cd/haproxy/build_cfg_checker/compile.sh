#!/bin/bash
cd haproxy

for i in $(git tag); do
        git checkout $i
        CFLAGS="-O2 -g -fno-strict-aliasing" make -j24 -pipe TARGET=linux2628 USE_LINUX_TPROXY=1 USE_ZLIB=1 USE_REGPARM=1 USE_PCRE2_STATIC=1 USE_PCRE2_JIT=1 USE_OPENSSL=1 USE_SYSTEMD=1 USE-NS=1 USE_LUA=1
        if [ "$?" == "0" ]; then
                cp haproxy ../haproxy-compiled/haproxy-$i
                make clean
        else
                printf "Error compiling haproxy $i\n"
        fi
done
