# Initial system setup

Initial system setup covers the server setup stage from when the SSH
connection first becomes active on an installed system to when it
is hardened and ready to be used.
