#!/bin/bash

DOMAINS=(test.example.com \
        )

for domain in ${DOMAINS[@]}; do
    cat /etc/letsencrypt/live/$domain/fullchain.pem \
        /etc/letsencrypt/live/$domain/privkey.pem > \
        /etc/pki/haproxy/$domain.pem
done



CONFSTATUS=$(haproxy -c -V -f /etc/haproxy/configs-enabled/web.cfg)

if [ "$CONFSTATUS" == "Configuration file is valid" ]
then
	systemctl reload haproxy@web
else
	printf "Error in haproxy configuration, refusing to reload!\n"
fi
