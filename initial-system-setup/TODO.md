# Todo list
 - set DNS servers to openDNS
 - initial firewalld whitelist (monitoring, bastion host etc.)
 - fix systemd bugs (set RemoveIPC=no and run systemctl restart systemd-logind.service)
 - disable cockpit on fedora and remove firewalld whitelist for it
 - kernel upgrades playbook
 - hardening ssh playbook
 - IDS, auditing and alerts
